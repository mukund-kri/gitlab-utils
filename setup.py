import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()


setuptools.setup(
    name='mbutil',
    version='0.0.2',
    author='Mukund K',
    author_email='mukund.kri@gmail.com',
    description='Setup and manage github groups for bootcamps',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/mukund-kri/gitlab-utils',
    package_dir={'': 'src'},
    packages=setuptools.find_packages(),

    scripts=[
        'scripts/db_cleaner',
        'scripts/devenv',
        'scripts/mbutil',
    ],
    install_requires=[
        'click==7.1.2',
        'python-dotenv==0.15.0',
        'python-gitlab==2.5.0',
        'GitPython==3.1.11',
        'tabulate==0.8.7',
        'prompt-toolkit==1.0.14',
        'psycopg2==2.8.6',
        'PyInquirer==1.0.3',
        'discord==1.0.1',
        'discord.py==1.6.0',
    ]
)
