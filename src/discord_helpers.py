from  datetime import datetime, timedelta

import click
import gitlab
import discord

import datastore



DISCORD_MESSAGE = ' you have not pushed any code today. Please make sure' + \
' push code to GitLab everyday.'

OFFENCES_YES_MESSAGE = '''
More then one student has failed. Sending the following message ...
'''


class GitPushComplience(discord.Client):
    ''' This tool finds out if all the students push code to gitlab 
    regularly '''
    
    def __init__(self, gitlab_url, private_token, live=False):
        self.gitlab_url = gitlab_url
        self.private_token = private_token
        self.live = live
        self.guild_id = datastore.get('GUILD_ID')
        super().__init__()
        
        
    async def on_ready(self):
        ''' Connect to discord and print out details and also send a test message
        to discord '''
        
        lst = self.__account_gitlab_pushes()

        if lst:
            msg = ", ".join(lst) + DISCORD_MESSAGE
        
            guild = self.get_guild(self.guild_id)

            click.secho(OFFENCES_YES_MESSAGE, fg='red')
            click.secho(msg, fg='green')

            if self.live:
                for channel in guild.text_channels:
                    if channel.name == 'general':
                        print(channel.name)
                        await channel.send(msg)

        else:
            print('All in complience')


    def __account_gitlab_pushes(self):
        ''' Gather commit info for all the students in the past day '''
        gl = gitlab.Gitlab(
            self.gitlab_url,
            private_token=self.private_token,
        )

        students = datastore.get('STUDENTS')
        non_complient = []

        click.secho()
        for _, student in students.items():
            user_id = '@' + student.gitlab_id

            users = gl.users.list(search=user_id)
            
            if not users:
                click.secho(f"User with GitLab id {user_id} not found", fg='red')
                continue
            
            user = users[0]
            now = datetime.now()
            today = now - timedelta(hours=48)
            events = user.events.list(action='pushed', after=today)
            
            if not events:
                click.secho(f"User {user.name} has no commits, adding to log", fg='green')
                non_complient.append(student.full_name)

        return non_complient


class TestGuild(discord.Client):
    ''' Tool to check if the bot is working and the correct discord channel is
    set '''
    
    def __init__(self, guild_id, live=False):
        self.guild_id = guild_id
        self.live = live
        super().__init__()
        
    
    async def on_ready(self):
        
        click.secho("\nGUILDS ASSOCIATED WITH BOT :: \n", fg='green', bold=True)
        for guild in self.guilds:
            print(guild.id, guild.name)

        guild = self.get_guild(self.guild_id)
        click.secho("\nCOHORT GUILD :: \n", fg='green')
        click.secho(guild.name)
        
        for channel in guild.channels:
            if channel.name == 'general':
                click.secho("\nDefault channel to which all messages go ::\n", fg='green')
                click.secho(channel.name)
                if self.live:
                    await channel.send("Testing")
        print()
