import pickle
from pathlib import Path

import settings


def save(key, value):
    ''' Add or override a key value pair in the project data store
    (pickle file)

    Parameters:
    key (string) : key of stored data
    value : actual data to be stored
    '''

    datafile = Path('.') / settings.DATA_DIR / settings.DATA_FILE

    with datafile.open('rb') as fp:
        data = pickle.load(fp)

    with datafile.open('wb') as fp:
        data[key] = value
        pickle.dump(data, fp)


def get(key):
    ''' Gat a key stored in the data store (pickle file)

    Parameters:
    key (string) : Lookup key
    '''

    datafile = Path('.') / settings.DATA_DIR / settings.DATA_FILE

    with datafile.open('rb') as fp:
        data = pickle.load(fp)

    return data.get(key, {})


def getfull():
    ''' Read and return the full data store structure '''
    
    datafile = Path('.') / settings.DATA_DIR / settings.DATA_FILE
    with datafile.open('rb') as fp:
        data = pickle.load(fp)
    return data
    
