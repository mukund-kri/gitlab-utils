import pickle

import click
from pathlib import Path

import settings


def init_cohort(name):
    ''' Initialize a cohort. Create all config files and directory strutures

    Parameters:
    name (string) : the name of cohort
    '''

    # create the "Data Base" pickle file
    datadir = Path('.') / settings.DATA_DIR
    datadir.mkdir(parents=True, exist_ok=True)

    # Initialize a barebones config
    config = {
        'COHORT_NAME': name,
        'GITLAB_GROUP': {},
        'STUDENTS': {},
        'PROJECTS': {},
    }

    datafile = datadir / settings.DATA_FILE

    if datafile.exists():
        click.echo('Project already initialized, exiting')
        return

    with datafile.open('wb') as f:
        pickle.dump(config, f)
