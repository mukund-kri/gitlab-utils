import os
from pathlib import Path

import click
from dotenv import load_dotenv


def load_context(ctx):
    env_path = Path('.') / '.env'
    load_dotenv(env_path)

    discord_token = os.getenv('DISCORD_TOKEN')
    if not discord_token:
        click.secho("\nDISCORD TOKEN NOT SET. PLEASE ADD TO .env\n", fg='red')
        exit()

    ctx.obj['gitlab_url'] = os.getenv('GITLAB_URL')
    ctx.obj['gitlab_token'] = os.getenv('GITLAB_PRIVATE_TOKEN')
    ctx.obj['gitlab_master_group'] = os.getenv('GITLAB_MASTER_GROUP')
    ctx.obj['discord_token'] = discord_token

    
