

class Student():

    def __init__(self, student_dict):
        self.nick = student_dict['Nickname']
        self.full_name = student_dict['Full Name']
        self.email = student_dict['Email']
        self.gitlab_id = student_dict['Gitlab_Id']

    def __str__(self):
        return f"<{self.full_name}>"

    def print_details(self):
        print(f"{self.nick} :: {self.full_name} {self.email} {self.gitlab_id}")

    def data(self):
        return (self.nick, self.full_name, self.email, self.gitlab_id)
