'''
Get all the forks of a project. That way I can get all the student
projects
'''

from os import path

import git
import click
import gitlab
from git import Repo

import datastore


def is_git_repo(path):
    ''' Checks if a given path is a repository.

    Parameters:
    path (string): The path of the directory to check

    Returns:
    Repo: if repo exists it returns a gitlab.Repo object, else it returns None
    '''

    try:
        repo = Repo(path)
        return repo
    except git.exc.InvalidGitRepositoryError:
        return None
    except:
        return None


def clone_or_pull(remote_url, target_dir, student):
    ''' Clones a remote git repo if it does not exist on disk. If it exits
    then it pulls the latest

    Parameters:
    remote_url (string): The url of remote git repo
    target_dir (string): The local directory to which the repo will be cloned
    '''

    repo = is_git_repo(target_dir)

    if repo:
        repo.remotes.origin.pull()
        print(f"Successfully Pulled latest from {student}'s repo")
    else:
        repo = Repo.clone_from(remote_url, target_dir)
        print(f"Successfully Cloned {student}'s repo")


def clone_all_forks(gitlab_url, private_token, master_project_id):
    ''' clone all forks of the master repo of assignment 

    Parameters:
    gitlab_url (string) : Url of gitlab server
    private_token (string) : Grivate access token to gitlab 
    master_project_id (int) : ID of repo who's forks we hav to download
    '''

    gl = gitlab.Gitlab(
        gitlab_url,
        private_token=private_token,
    )
    project = gl.projects.get(master_project_id)

    # Loop through all the forks of the master repo
    for p in project.forks.list():
        path_elements = p.path_with_namespace.split('/')
        target_dir = path.join(path_elements[3], path_elements[2])

        try:
            clone_or_pull(p.ssh_url_to_repo, target_dir, path_elements[2])
        except Exception as e:
            click.secho(f"Clone or Pull failed for {path_elements[2]}. Fix manually")
            print(e)

def students_permissions_for_group(gitlab_url, gitlab_token, group_id):
    ''' Set the persmission of all the students to Developer for this group 

    Parameters:
    gitlab_url (string) : Url of gitlab server
    gitlab_token (string) : Private access token to gitlab 
    group_id (int) : ID of group to set persmission for
    '''

    gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
    group = gl.groups.get(group_id)
    students = datastore.get('STUDENTS')


    for _, student in students.items():
        gid = student.gitlab_id
        # Get the user to add as owner
        users = gl.users.list(search=gid)


        if users:
            user = users[0]

            try:
                group.members.create({
                    'user_id': user.id,
                    'access_level': gitlab.DEVELOPER_ACCESS
                })
            except Exception as e:
                print(e)

                
def set_group_permissions(ctx, group):
    if group == 'root':
        group_id = datastore.get('GITLAB_GROUP')
        students_permissions_for_group(
            ctx.obj['GITLAB_URL']
        )


def create_group(gitlab_url, gitlab_token, name, parent_group_id):
    ''' Create a ``gitlab sub group`` within the parent group.
    
    Parameters:
    gitlab_url (string) : Url of the gitlab server
    gitlab_token (string) : Private access token of gitlab
    name (string) : Name of the new group 
    parent_group_id (int) : ID of the parent gitlab group
    '''

    gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
    try:
        group = gl.groups.create({
            'name': name,
            'path': name,
            'parent_id': parent_group_id,
        })
        return group
    except Exception as e:
        print(e)


def create_student_groups(gitlab_url, private_token):
    ''' Create a seperate group for each user 

    Parameters:
    gitlab_url (string) : Url of the gitlab server
    gitlab_token (string) : Private access token of gitlab
    '''
    
    gl = gitlab.Gitlab(gitlab_url, private_token=private_token)
    students = datastore.get('STUDENTS')
    cohort_group = datastore.get('GITLAB_COHORT_GROUP')
    groups = gl.groups

    for _, student in students.items():
        user = student.full_name
        gitlab_id = '@' + student.gitlab_id

        path = user.lower().replace(' ', '_')
        click.secho(f"Creating user group :: {user} ", fg='green')

        try:
            group = groups.create({
                'name': user,
                'path': path,
                'parent_id': cohort_group,
            })

            # Get the user to add as owner
            users = gl.users.list(search=gitlab_id)

            if users:
                user = users[0]
                group.members.create({
                    'user_id': user.id,
                    'access_level': gitlab.OWNER_ACCESS
                })

        except Exception as e:
            print(e)



