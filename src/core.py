import os

import click


PLUGIN_FOLDER = os.path.join(os.path.dirname(__file__), 'commands')


class GitLabCli(click.MultiCommand):
    ''' The entry point to the Mentor Util CLI app '''

    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(PLUGIN_FOLDER):
            if filename.endswith('.py'):
                rv.append(filename[:-3])
        rv.sort()
        return rv


    def get_command(self, ctx, name):
        ns = {}
        fn = os.path.join(PLUGIN_FOLDER, name + '.py')
        with open(fn) as f:
            code = compile(f.read(), fn, 'exec')
            eval(code, ns, ns)
        return ns['cli']
