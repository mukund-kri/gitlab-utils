import os
from pathlib import Path

import click
import gitlab
from tabulate import tabulate
from dotenv import load_dotenv

import datastore
from git_helpers import clone_all_forks
from PyInquirer import prompt


@click.group()
@click.pass_context
def cli(ctx):
    ''' Manage repos related to student projects '''

    # Load the .env file. Needed for gitlab url and token
    env_path = Path('.') / '.env'
    load_dotenv(env_path)
    ctx.ensure_object(dict)
    ctx.obj['url'] = os.getenv('GITLAB_URL')
    ctx.obj['token'] = os.getenv('GITLAB_PRIVATE_TOKEN')



@cli.command()
def list():
    ''' List all the projects and its gitlab root repo ids '''

    projects = datastore.get('PROJECTS')
    projects = [(ref, repo['repo_id'], repo['name'], repo['description']) \
                for ref, repo in projects.items()]

    click.secho('\nCOHORT PROJECTS\n', fg='green', bold=True)
    print(tabulate(
        projects,
        headers=['Ref', 'Repo ID', 'Name', 'Description'],
        showindex=True,
    ))
    print()


@cli.command()
@click.argument('name')
@click.argument('repo_id', type=click.INT)
@click.pass_context
def add(ctx, name, repo_id):
    ''' Add a new project and the id of the master repo '''

    gl = gitlab.Gitlab(
        ctx.obj['url'],
        private_token=ctx.obj['token'],
    )

    repo = gl.projects.get(repo_id)
    
    projects = datastore.get('PROJECTS')
    projects[name] = {
        'repo_id': repo_id,
        'name': repo.name,
        'description': repo.description,
    }
    datastore.save('PROJECTS', projects)



def __pull_project_repos(project, ctx):
    ''' Driver code to pull all the student repos. 
    Use by both the click and PyInquirer frontends 

    Parameters
    ----------
    project : Dict 
    Dict with project data

    ctx : ClickContext
    The click context
    '''
    
    click.secho(f"\nCLONING / PULLING ALL STUDENTS REPOS FOR PROJECT {project['name']}",
                fg='green', bold=True)
    click.secho('This is going to take some time be patient\n', fg='green')
    clone_all_forks(
        ctx.obj['url'],
        ctx.obj['token'],
        project['repo_id'],
    )
    
@cli.command()
@click.argument('project_name')
@click.pass_context
def pull(ctx, project_name):
    ''' Pull all the projects student repos '''
    projects = datastore.get('PROJECTS')

    project = projects.get(project_name)
    if not project:
        click.secho(f'Project name does not exits. Please check\n', fg='red')
        exit()

    __pull_project_repos(project, ctx)


@cli.command()
@click.pass_context
def p2(ctx):
    ''' Pull all the student repos (PyInquirer UI) '''
    projects = datastore.get('PROJECTS')
    project_names = [{'name': project} for project, _ in projects.items()]

    select_project = [
        {
            'type': 'list',
            'name': 'project',
            'message': 'Select the project you want pull update code from',
            'choices': project_names,
        }
    ]

    answer = prompt(select_project)
    target = answer['project']
    target_project = projects.get(target)

    __pull_project_repos(target_project, ctx)
