from pathlib import Path

import click
import gitlab
import discord

import settings
import datastore
from cohort_helpers import init_cohort
from click_helpers import load_context
from discord_helpers import TestGuild, GitPushComplience


NO_ENV_ERR = '''
.env file does not exist in the current directory. Create a .env file before 
you procede.

The .env file should have the following variables
GITLAB_URL           :: usually gitlab.com 
GITLAB_MASTER_GROUP  :: The root group of bootcamp (NOT root of cohort)
GITLAB_PRIVATE_TOKEN :: Gitlab token to access API

DISCORD_TOKEN        :: Token for discord API
'''    


@click.group()
@click.pass_context
def cli(ctx):
    ''' Manage the cohort folder and config '''
    ctx.ensure_object(dict)
    load_context(ctx)
   

@cli.command()
@click.argument('name')
def init(name):
    ''' Initialize current working directory as a cohort root '''
    
    # Check if the .env file exists
    env_path = Path('.') / '.env'
    if not env_path.is_file():
        click.secho(NO_ENV_ERR, fg="red")

    init_cohort(name)


@cli.command()
def info():
    ''' Display information on this cohort '''
    ds = datastore.getfull()

    click.secho(f"Cohort :: {ds['COHORT_NAME']}")
    print(ds)

@cli.command()
@click.argument('guild_id', type=click.INT)
def set_discord_guild(guild_id):
    ''' Set the guild for this cohort '''
    datastore.save('GUILD_ID', guild_id)


@cli.command()
@click.pass_context
def test_discord(ctx):
    token = ctx.obj['discord_token']
    guild_id = datastore.get('GUILD_ID')
    
    client = TestGuild(guild_id)
    client.run(token)

    
@cli.command()
@click.option('--live/--no-live', default=False)
@click.pass_context
def check_daily_pushes(ctx, live):
    gpc = GitPushComplience(
        ctx.obj['gitlab_url'],
        ctx.obj['gitlab_token'],
        live,
    )

    token = ctx.obj['discord_token']
    gpc.run(token)
