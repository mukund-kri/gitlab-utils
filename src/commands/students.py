import csv

import click
from tabulate import tabulate

import datastore
from models import Student


@click.group()
def cli():
    ''' Manage the students in a cohort '''
    pass


@cli.command()
@click.argument('nickname')
def rm(nickname):
    ''' Delete a student from the datastore '''
    students = datastore.get('STUDENTS')
    del(students[nickname])
    datastore.save('STUDENTS', students)
    

@cli.command()
def list():
    ''' List all students in datastore '''
    students = datastore.get('STUDENTS')
    students_data = [student.data() for student in students.values()]

    click.secho('\nSTUDENTS DETAILS\n', fg='green', bold=True)
    print(tabulate(
        students_data,
        headers=["Nickname", "Fullname", "Email", "Gitlab ID",],
        showindex=True,
    ))


@cli.command()
@click.argument('nickname')
def show(nickname):
    ''' Show the details of a particular student '''
    students = datastore.get('STUDENTS')
    student_data = students[nickname].data()

    click.secho(f'\nSTUDENTS DETAILS FOR {nickname}\n', fg='green', bold=True)
    print(tabulate(
        [student_data],
        headers=["Nickname", "Fullname", "Email", "Gitlab ID",]
    ))

    
@cli.command()
@click.option('source', '--source', default='students.csv', type=click.File())
def load(source):
    ''' (Re)Load students from csv file '''
    csv_data = csv.DictReader(source)
    from_source = { row['Nickname']: Student(row) for row in csv_data }
    datastore.save('STUDENTS', from_source)
    

@cli.command()
@click.argument('nickname')
@click.argument('field')
@click.argument('value')
def update(nickname, field, value):
    ''' Update a specific field of a student with given nickname'''

    students = datastore.get('STUDENTS')
    student = students.get(nickname)

    if not student:
        click.echo(f"No student with nickname {nickname} exists", fg='red')
        return

    if field == 'gitlab_id':
        student.gitlab_id = value

    datastore.save('STUDENTS', students)
