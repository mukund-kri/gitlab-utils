import click

from click_helpers import load_context
import datastore
from git_helpers import (
    create_group,
    students_permissions_for_group,
    create_student_groups,
)

@click.group()
@click.pass_context
def cli(ctx):
    ''' Manage the cohort folder and config '''
    ctx.ensure_object(dict)
    load_context(ctx)


@cli.command()
@click.pass_context
@click.argument('group', type=click.Choice(['cohort', 'templates']))
def permissions(ctx, group):
    if group == 'cohort':
        group_id = datastore.get('GITLAB_COHORT_GROUP')
    else:
        group_id = datastore.get('GITLAB_TEMPLATES_GROUP')
    students_permissions_for_group(
        ctx.obj['gitlab_url'],
        ctx.obj['gitlab_token'],
        group_id,
    )

    
@cli.command()
@click.pass_context
@click.argument('group', type=click.Choice(['cohort', 'templates', 'students']))
def create_groups(ctx, group):
    ''' Create COHORT, TEMPLATES or STUDENTS groups '''
    if group == 'cohort':
        click.secho('\nCREATING COHORT GROUP\n', fg='green', bold=True)
        gl_group = create_group(
            ctx.obj['gitlab_url'],
            ctx.obj['gitlab_token'],
            datastore.get('COHORT_NAME'),
            ctx.obj['gitlab_master_group'],
        )
        datastore.save('GITLAB_COHORT_GROUP', gl_group.id)
    elif group == 'templates':
        click.secho('\nCREATING TEMPLATES GROUP\n', fg='green', bold=True)
        gl_group = create_group(
            ctx.obj['gitlab_url'],
            ctx.obj['gitlab_token'],
            'Templates',
            datastore.get('GITLAB_COHORT_GROUP')
        )
        datastore.save('GITLAB_TEMPLATES_GROUP', gl_group.id)
    else:
        click.secho("\nCREATING STUDENTS' GROUP\n", fg='green', bold=True)
        create_student_groups(
            ctx.obj['gitlab_url'],
            ctx.obj['gitlab_token'],
        )
    
    
@cli.command()
@click.argument('key', type=click.Choice(['cohort', 'templates']))
@click.argument('value', type=click.INT)
def set_gitid(key, value):
    if key == 'master':
        datastore.set('GITLAB_COHORT_GROUP', value)
    else:
        datastore.set('GITLAB_TEMPLATES_GROUP', value)
