import os
import venv
from pathlib import Path

import datastore


def create_venv(name):
    ''' Create a new virtualenv in virtualenvtool's directory '''
    workon_home = os.getenv('WORKON_HOME')

    venv_path = Path(workon_home) / name
    venv.create(venv_path, with_pip=True, clear=False)
    
    print(f'Created virtualenv {name}')


def create_student_venvs():
    ''' Create the virtualenv for all students in given cohort '''
    students = datastore.get('STUDENTS')
    for _, student in students.items():
        venv_name = student.full_name.replace(' ', '_').lower()
        create_venv(venv_name)

