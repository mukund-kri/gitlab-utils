# MB mentor utils

## Command gitutil.py
This tool takes care of creating github subgroups for each student in which they
will work. It also provides a few utils for mass download of student submissions.

### Sub commands

#### init

Prepares the current directory as a cohort root.

#### load_students

Load all the student details from a csv file into a pickle file. This serves as
a "database" for the util.


#### cohort_group

Create a repo


## Command devenv.py


## Command db_cleaner.py
